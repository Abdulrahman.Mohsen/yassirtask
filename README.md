# Yassir Task

This project is a coding task by **Abdelrahman mohsen**.   
Technical Task for **Yassir**.

# Brief For Task

- Retrieve Movies from requirements task endpoint.
- Match list include movie name, image and release date.
- Match details include movie name, image, release date and movie overview.
- Adding sample of unit test and the behavior to be testabe.

# Architecture

 Task flow with viper and this contain.
- View: displays what it is told to by the Presenter and relays user input back to the Presenter. 
- Interactor: contains the business logic as specified by a use case. e.g if business logic depend on making network calls then it is Interactor’s responsibility to do so
- Presenter: contains view logic for preparing content for display (as received from the Interactor) and for reacting to user inputs (by requesting new data from the Interactor).
- Entity: contains basic model objects used by the Interactor.
- Routing: contains navigation logic for describing which screens are shown in which order.







