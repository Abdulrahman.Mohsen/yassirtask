//
//  InteractorStub.swift
//  YassirTaskTests
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation
@testable import YassirTask

final class MovieListInteractorStub: MovieListInteractorProtocol {
    var movieListResultToBeReturn: MovieListResult!

    func getMovieList(handler: @escaping (MovieListResult) -> Void) {
        handler(movieListResultToBeReturn)
    }
}
