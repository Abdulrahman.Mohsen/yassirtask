//
//  MovieListPresenterTests.swift
//  YassirTaskTests
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import XCTest
@testable import YassirTask

class MovieListPresenterTests: XCTestCase {
    var viewSpy: MovieListViewSpy!
    var stub: MovieListInteractorStub!
    var routerSpy: MovieListRouterSpy!
    var presenter: MovieListPresenter!
    var cellSpy: MovieListCellSpy!
    override func setUp() {
        super.setUp()
        viewSpy = MovieListViewSpy()
        routerSpy = MovieListRouterSpy()
        stub = MovieListInteractorStub()
        cellSpy = MovieListCellSpy()
        presenter = MovieListPresenter(view: viewSpy, interactor: stub, router: routerSpy)
    }
    override func tearDown() {
        viewSpy = nil
        stub = nil
        cellSpy = nil
        presenter = nil
        super.tearDown()
    }
    func testFetchDataSuccess() {
        // Given
        let movieModel = MovieModel(id: 0, overview: "overview", posterPath: "/path", releaseDate: "10-10-2010", title: "title")
        let items = [MovieItem(item: movieModel)]
        let indexPath = IndexPath(row: 0, section: 0)
        let expectedItem = items[indexPath.row]
        stub.movieListResultToBeReturn = .success(items)
        // When
        presenter.fetchData()
        presenter.configure(cell: cellSpy, for: indexPath)
        // Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.reloadDataCallCount, 1)
        XCTAssertEqual(presenter.numberOfRows, items.count)
        XCTAssertEqual(cellSpy.configureCallCount, 1)
        XCTAssertEqual(cellSpy.passedItem, expectedItem)
    }
    func testFetchDataFail() {
        // Given
        let error = ApiError.missingData
        stub.movieListResultToBeReturn = .failure(error)
        // When
        presenter.fetchData()
        // Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.showErrorCallCount, 1)
        XCTAssertEqual(viewSpy.passedError, error.localizedDescription)
    }
    func testDidSelectRow() {
        // Given
        let movieModel = MovieModel(id: 0, overview: "overview", posterPath: "/path", releaseDate: "10-10-2010", title: "title")
        let items = [MovieItem(item: movieModel)]
        let indexPath = IndexPath(row: 0, section: 0)
        let expectedItem = items[indexPath.row]
        stub.movieListResultToBeReturn = .success(items)
        // When
        presenter.fetchData()
        presenter.didSelectRowAt(indexPath: indexPath)
        // Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.reloadDataCallCount, 1)
        XCTAssertEqual(presenter.numberOfRows, items.count)
        XCTAssertEqual(routerSpy.showMovieDetailsCallCount, 1)
        XCTAssertEqual(routerSpy.passedItem, expectedItem)
    }

    func testNumberOfRows() {
        // Given
        let movieModel = MovieModel(id: 0, overview: "overview", posterPath: "/path", releaseDate: "10-10-2010", title: "title")
        let items = [MovieItem(item: movieModel),
                     MovieItem(item: movieModel)]
        stub.movieListResultToBeReturn = .success(items)
        // When
        presenter.fetchData()
        // Then
        XCTAssertEqual(presenter.numberOfRows, 2)
    }
}
