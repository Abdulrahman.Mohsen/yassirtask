//
//  RouterSpy.swift
//  YassirTaskTests
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation
@testable import YassirTask

final class MovieListRouterSpy: MovieListRouterProtocol {
    var showMovieDetailsCallCount = 0
    var passedItem: MovieItem?
    func showMovieDetails(item: MovieItem) {
        showMovieDetailsCallCount += 1
        passedItem = item
    }
}
