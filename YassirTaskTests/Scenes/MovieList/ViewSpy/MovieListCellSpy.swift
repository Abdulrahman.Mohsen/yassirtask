//
//  MovieListCellSpy.swift
//  YassirTaskTests
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation
@testable import YassirTask

final class MovieListCellSpy: MovieListCellPresentable {
    var configureCallCount = 0
    var passedItem: MovieItem?
    func configure(item: MovieItem) {
        configureCallCount += 1
        passedItem = item
    }
}
