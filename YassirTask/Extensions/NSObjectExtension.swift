//
//  NSObjectExtension.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation

extension NSObject {
    class var className: String { "\(self)" }
}
