//
//  UITableViewCellExtensions.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit

extension UITableViewCell {
    class var cellId: String { "\(self)" }
}
