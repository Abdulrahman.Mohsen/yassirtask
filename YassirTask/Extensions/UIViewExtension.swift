//
//  UIViewExtension.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit

extension UIView {
    static var nib: UINib {
         UINib(nibName: self.className, bundle: nil)
    }
}


