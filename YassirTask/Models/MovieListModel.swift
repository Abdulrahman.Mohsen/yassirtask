//
//  MovieListModel.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation
struct MovieCollection: Codable {
    let results: [MovieModel]
}

struct MovieModel: Codable {
    let id: Int
    let overview: String
    let posterPath, releaseDate, title: String

    enum CodingKeys: String, CodingKey {
        case id
        case overview
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title
    }
}
