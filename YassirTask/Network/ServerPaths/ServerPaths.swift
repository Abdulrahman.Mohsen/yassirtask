//
//  ServerPaths.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation

public enum ServerPaths: String {
    case movieList = "discover/movie"
    private var baseURL: String { "https://api.themoviedb.org/3/" }
    var value: String { baseURL + rawValue }
}
