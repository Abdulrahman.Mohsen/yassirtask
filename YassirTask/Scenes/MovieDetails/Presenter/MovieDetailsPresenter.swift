//
//  MovieDetailsPresenter.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation

protocol MovieDetailsPresentation: AnyObject {
    func configureView(with item: MovieItem)
}

protocol MovieDetailsPresenterProtocol {
    func viewDidload()
}

final class MovieDetailsPresenter: MovieDetailsPresenterProtocol {
    private weak var view: MovieDetailsPresentation!
    private var item: MovieItem!
    init(
        view: MovieDetailsPresentation,
        item: MovieItem
    ) {
        self.view = view
        self.item = item
    }
    func viewDidload() {
        view.configureView(with: item)
    }
}
