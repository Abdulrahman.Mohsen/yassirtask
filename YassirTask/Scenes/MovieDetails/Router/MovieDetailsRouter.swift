//
//  MovieDetailsRouter.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit

class MovieDetailsRouter {
    static func createMovieDetails(item: MovieItem) -> UIViewController {
        let controller = MovieDetailsViewController()
        let presenter = MovieDetailsPresenter(view: controller, item: item)
        controller.presenter = presenter
        return controller
    }
}
