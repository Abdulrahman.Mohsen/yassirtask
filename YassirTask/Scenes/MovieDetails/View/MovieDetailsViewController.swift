//
//  MovieDetailsViewController.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit

final class MovieDetailsViewController: UIViewController {

    // MARK: - outlets
    @IBOutlet private weak var movieOverView: UITextView!
    @IBOutlet private weak var movieReleaseDate: UILabel!
    @IBOutlet private weak var movieTitle: UILabel!
    @IBOutlet private weak var movieImage: RemoteImageView!
    // MARK: - variables
    var presenter: MovieDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Details"
        presenter.viewDidload()
    }

}
// MARK: - MovieDetailsPresentation
extension MovieDetailsViewController: MovieDetailsPresentation {
    func configureView(with item: MovieItem) {
        movieImage.loadImage(urlString: item.posterPath)
        movieTitle.text = item.title
        movieReleaseDate.text = item.releaseDate
        movieOverView.text = item.overview
    }
}
