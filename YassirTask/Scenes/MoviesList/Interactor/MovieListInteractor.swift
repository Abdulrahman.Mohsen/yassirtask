//
//  MovieListInteractor.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit

typealias MovieListResult = (Result<[MovieItem], Error>)

protocol MovieListInteractorProtocol {
    func getMovieList(handler: @escaping (MovieListResult) -> Void )
}

final class MovieListInteractor: MovieListInteractorProtocol {
    func getMovieList(handler: @escaping (MovieListResult) -> Void) {
        NetworkManager.request(model: MovieCollection.self, url: .movieList, parameters: ["api_key":"c9856d0cb57c3f14bf75bdc6c063b8f3"]) { result in
            let handlerMovie = result.map({movies in
                movies.results.map(MovieItem.init)
            })
            handler(handlerMovie)
        }
    }
}
