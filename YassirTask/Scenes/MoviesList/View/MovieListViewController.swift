//
//  MovieListViewController.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit

final class MovieListViewController: UIViewController {

    // MARK: - outlets
    @IBOutlet private weak var tableView: UITableView!
    // MARK: - variables
    var presenter: MovieListPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Movies List"
        setupTableView()
        presenter.fetchData()
    }
    private func setupTableView() {
        tableView.register(cell: MovieListCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
}

// MARK: - MovieListPresentation
extension MovieListViewController: MovieListPresentation {
    func showLoader() {
        // MARK: - add loader
    }
    func dismissLoader() {
        // MARK: - dismiss loader
    }
    func showError(error: String) {
        print(error)
    }
    func reloadData() {
        tableView.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         presenter.numberOfRows
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MovieListCell = tableView.dequeueCell(for: indexPath)
        presenter.configure(cell: cell, for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath: indexPath)
    }
}
