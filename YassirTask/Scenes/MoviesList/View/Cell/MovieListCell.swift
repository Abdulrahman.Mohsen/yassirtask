//
//  MovieListCell.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit
protocol MovieListCellPresentable {
    func configure(item: MovieItem)
}
class MovieListCell: UITableViewCell {

    // MARK: - outlets
    @IBOutlet private weak var movieName: UILabel!
    @IBOutlet private weak var movieReleaseDate: UILabel!
    @IBOutlet private weak var movieImage: RemoteImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        movieImage.image = nil
    }
    
}

extension MovieListCell: MovieListCellPresentable{
    func configure(item: MovieItem) {
        movieName.text = item.title
        movieReleaseDate.text = item.releaseDate
        movieImage.loadImage(urlString: "https://www.image.tmdb.org/t/p/w200" + item.posterPath)
    }
}
