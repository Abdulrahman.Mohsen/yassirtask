//
//  MovieItem.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation
struct MovieItem: Equatable {
    let id: Int
    let overview: String
    let posterPath, releaseDate, title: String
    
    static func == (lhs: MovieItem, rhs: MovieItem) -> Bool { lhs.id == rhs.id}
}

extension MovieItem {
    init(item: MovieModel) {
        self.id = item.id
        self.overview = item.overview
        self.posterPath = item.posterPath
        self.releaseDate = item.releaseDate
        self.title = item.title
    }
}
