//
//  MovieListPresenter.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import Foundation

protocol MovieListPresentation: AnyObject {
    func showLoader()
    func dismissLoader()
    func showError(error: String)
    func reloadData()
}

protocol MovieListPresenterProtocol {
    var numberOfRows: Int { get }
    func fetchData()
    func configure(cell: MovieListCellPresentable, for indexPath: IndexPath )
    func didSelectRowAt(indexPath: IndexPath)
}

final class MovieListPresenter {
    private weak var view: MovieListPresentation!
    private var interactor: MovieListInteractorProtocol!
    private var router: MovieListRouterProtocol!
    private var items = [MovieItem]() { didSet { view.reloadData() } }
    init(
        view: MovieListPresentation,
        interactor: MovieListInteractorProtocol,
        router: MovieListRouterProtocol
    ) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}
// MARK: - MovieListPresenterProtocol
extension MovieListPresenter: MovieListPresenterProtocol {
    
    var numberOfRows: Int { items.count }
    func configure(cell: MovieListCellPresentable, for indexPath: IndexPath ) {
        cell.configure(item: items[indexPath.row])
    }
    func didSelectRowAt(indexPath: IndexPath) {
        router.showMovieDetails(item: items[indexPath.row])
    }
    
    func fetchData() {
        view.showLoader()
        interactor.getMovieList { [weak self] result in
            guard let self = self else { return }
            self.view.dismissLoader()
            switch result {
            case .success(let model):
                self.items = model
            case .failure(let error):
                print(error)
                self.view.showError(error: error.localizedDescription)
            }
        }
    }
}
