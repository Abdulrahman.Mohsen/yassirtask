//
//  MovieListRouter.swift
//  YassirTask
//
//  Created by Abulrahman mohsen on 17/12/2022.
//

import UIKit
protocol MovieListRouterProtocol: AnyObject {
    func showMovieDetails(item: MovieItem)
}

final class MovieListRouter {
    private weak var viewController: MovieListViewController?
    init(viewController: MovieListViewController) {
        self.viewController = viewController
    }
    static func createMovieList() -> UIViewController {
        let controller = MovieListViewController()
        let interactor = MovieListInteractor()
        let router = MovieListRouter(viewController: controller)
        let presenter = MovieListPresenter(view: controller, interactor: interactor, router: router)
        controller.presenter = presenter
        return controller
    }
}

extension MovieListRouter: MovieListRouterProtocol {
    func showMovieDetails(item: MovieItem) {
        let movieDetailsViewController = MovieDetailsRouter.createMovieDetails(item: item)
        viewController?.show(movieDetailsViewController, sender: nil)
    }
}
